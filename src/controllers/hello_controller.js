import { Controller } from 'stimulus'

function HelloController (context) {
  Controller.call(this, context)
  Object.defineProperty(
    this,
    'name',
    {
      get: function () {
        return this.nameTarget.value
      }
    }
  )
}

HelloController.prototype = Object.create(Controller.prototype, {
  connect: {
    value: function () { console.log('Hello World') }
  },
  copy: {
    value: function () {
      this.nameTarget.select()
      document.execCommand('copy')
      console.log('copied to clipboard', this.name)
    }
  },
  disconnect: {
    value: function () { console.log('bye') }
  },
  constructor: {
    value: HelloController,
    configurable: false
  }
})

HelloController.bless = Controller.bless
HelloController.targets = [ 'name' ]

export default HelloController
